import './App.css';

import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollectionData } from 'react-firebase-hooks/firestore'

import React, { useState } from "react";

firebase.initializeApp({
  apiKey: "AIzaSyAbN7TP81E5FlAfVqp8oUFd4Zr5ib8mZk4",
  authDomain: "seminario-78242.firebaseapp.com",
  projectId: "seminario-78242",
  storageBucket: "seminario-78242.appspot.com",
  messagingSenderId: "618674589922",
  appId: "1:618674589922:web:cf144d068f4bc8501b8ebb"
})

const auth = firebase.auth();
const firestore = firebase.firestore();

function App() {
  const [user] = useAuthState(auth);

  return (
    <div className="App">
      <header>
        <Sair />
      </header>
      <section>
        {user ? <Chat /> : <Logar />}
      </section>
    </div>
  );
}

function Logar() {
  const logarGoogle = () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider);
  }

  return(
    <button onClick={logarGoogle}>Entre com o google</button>
  )
}

function Sair() {
  return auth.currentUser && (
    <button onClick={()=> auth.signOut()}>Sair</button>
  )
}

function Chat() {
  const mensagemRef = firestore.collection('mensagens');
  const query = mensagemRef.orderBy('createdAt').limit(25);

  const [mensagens] = useCollectionData(query, {idField: 'id'});

  const [formValue, setFormValue] = useState('');

  const enviarMensagem = async(e) => {
    e.preventDefault();
    const { uid } = auth.currentUser;

    await mensagemRef.add({
      text: formValue,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      uid
    });
    setFormValue('');
  }

  return(
    <>
      <main>
        {mensagens && mensagens.map(msg => <ChatMensagens key={msg.id} mensagem={msg}/>)}
      </main>

      <form onSubmit={enviarMensagem}>
        <input value={formValue} onChange={(e) => setFormValue(e.target.value)}/>
        <button type="submit">Enviar</button>
      </form>
    </>
  )
}

function ChatMensagens(props) {
  const { text, uid } = props.mensagem;
 
  const mensagemClass = uid === auth.currentUser.uid ? 'sent' : 'received'

  return (
    <div className={`mensagem ${mensagemClass}`}>
      <p>{text}</p>
    </div>
  )
}

export default App;
